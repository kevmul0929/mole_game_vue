import axios from 'axios';
import Errors from './Errors';

class Form {
  /**
   * @param  {data}
   */
  constructor(data) {
    this.originalData = data;

    Object.keys(data).forEach((field) => {
      this[field] = data[field];
    });

    this.errors = new Errors();
  }


  /**
   * @return {data object}
   */
  data() {
    const data = {};
    this.originalData.forEach((property) => {
      data[property] = this[property];
    });

    return data;
  }


  /**
   * Submit a request with Axios
   *
   * @param  {request type[post, delete, put, update]}
   * @param  {url}
   *
   * @return {Promise}
   */
  submit(requestType, url) {
    return new Promise((resolve, reject) => {
      axios[requestType](url, this.data())
        .then((response) => {
          this.onSuccess(response.data);

          resolve(response.data);
        })
        .catch((errors) => {
          this.onFail(errors.response.data);

          reject(errors.response.data);
        });
    });
  }


  /**
   * Send a post request to the server
   *
   * @param  {url}
   */
  post(url) {
    this.submit('post', url);
  }


  /**
   * returns a successful message and resets the data
   *
   * @param  {data}
   */
  onSuccess() {
    this.reset();
  }


  /**
   * Records the errors if there is a failure during Submit
   *
   * @param  {errors}
   */
  onFail(errors) {
    this.errors.record(errors);
  }


  /**
   * Resets all the data
   *
   * @return {}
   */
  reset() {
    this.data.forEach((field) => {
      this[field] = '';
    });

    this.errors.clear();
  }
}

export default Form;
