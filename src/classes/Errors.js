class Errors {
  constructor() {
    this.errors = {};
  }

  has(field) {
    return Object.prototytpe.hasOwnProperty.call(this.errors, field);
  }

  any() {
    return Object.keys(this.errors).length > 0;
  }

  get(field) {
    if (this.errors[field]) {
      return this.errors[field][0];
    }
    return null;
  }

  record(errors) {
    this.errors = errors;
  }

  clear(field) {
    if (field) {
      return delete this.errors[field];
    }

    this.errors = '';
    return 'success';
  }
}

export default Errors;
