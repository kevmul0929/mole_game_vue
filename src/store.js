import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    score: 0,
    scores: {},
    holesCount: 6,
    chosenHole: null,
    lastMoleHit: null,
    lastHole: null,
    gameOver: false,
  },

  mutations: {
    increaseScore(state, mole) {
      this.state.score += 1;
      this.state.lastMoleHit = mole;
    },
    setHole(state, hole) {
      this.state.chosenHole = hole;
      this.state.lastHole = hole;
    },
    timeUp() {
      this.state.gameOver = true;
    },
    unsetMole() {
      this.state.chosenHole = null;
    },
    unsetLastMole() {
      this.state.lastMoleHit = null;
    },
    resetScore() {
      this.state.score = 0;
      this.state.gameOver = false;
    },
    updateScores(state, payload) {
      this.state.scores = payload;
    },
  },

  actions: {
    incrementScore(context, payload) {
      context.commit('increaseScore', payload);
      context.commit('unsetMole');
    },
    chooseHole(context, payload) {
      context.commit('setHole', payload.hole);
      // Unset the last mole after a set time
      setTimeout(() => context.commit('unsetLastMole'), payload.time);
    },
    endGame(context) {
      context.commit('timeUp');
      context.commit('unsetMole');
    },
    newGame(context) {
      context.commit('resetScore');
    },
    setScores(context, payload) {
      context.commit('updateScores', payload);
    },
  },
});
