import axios from 'axios';

const url = 'api/scores/';

class ScoreService {
  // Get Scores
  static get() {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await axios.get(url);
        console.log(data);
        resolve(
          data.map(score => ({
            ...score,
            createdAt: new Date(score.createdAt),
          })),
        );
      } catch (error) {
        reject(error);
      }
    });
  }

  static post(score) {
    console.log(score);
    return axios.post(url, { score });
  }

  static delete(id) {
    return axios.delete(`${url}${id}`);
  }
}

export default ScoreService;
