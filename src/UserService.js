import axios from 'axios';

const url = 'api/users/';

class UserService {
  // Get Users
  static get() {
    return new Promise(async (resolve, reject) => {
      try {
        const { data } = await axios.get(url);
        console.log(data);
        resolve(
          data.map(user => ({
            ...user,
            createdAt: new Date(user.createdAt),
          })),
        );
      } catch (error) {
        reject(error);
      }
    });
  }

  static post(user) {
    // TODO: Validation
    return axios.post(url, {
      name: user.name,
      email: user.email,
      password: user.password,
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  static delete(id) {
    return axios.delete(`${url}${id}`);
  }
}

export default UserService;
